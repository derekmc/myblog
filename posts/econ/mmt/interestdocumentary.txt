Interest Documentary

<p>I recorded this video a while back on interest rates.

<p>
The most important part of the video is the "counterfeit loan" thought experiment.
The "counterfeit loan" thought experiment works like this:

<ol> <li> You counterfeit money to give yourself a loan.</li>
     <li> You put radio trackers in the countefeit money, so you can later find it.</li>
     <li> Instead of "repaying" the loan, you track down your counterfeit money, and replace it with real money.</li>
     <li> Boom! you've given yourself a zero interest loan, and only committed 2 major felonies</li>
</ol>

<p> The point of the thought experiment is not to encourage counterfeiting,
but to provide another viewpoint of how loans actually work.  If you 
can create a "money substitute", like a cryptocurrency, a stablecoin,
a stock, etc -- you are well on your way to borrowing without interest,
as MMT asserts is true for monetary sovereigns.</p>

<p> So in reality, there is a gradient of being forced to pay interest for
credit, and there are practical things that you personally can do to move
up that gradient.  On the extreme end, is the ability to issue your own
currency, and if that currency is accepted everywhere, then
you have a zero interest line of credit. </p>

<p> But currently, not even apple shares or amazon shares are accepted as a means of payment.
This is likely due primarily to securities law. Anyway, it shows the the story 
of interest rates is not so plain and simple as it appears on the surface.</p>



<iframe width="500" height="320" src="https://www.youtube.com/embed/OdVLu-6QF0k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
